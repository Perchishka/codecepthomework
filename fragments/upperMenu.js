const {I} = inject();

module.exports = {

    accountButton: '//header[@role="banner"]//span[last()]//button',
    logoutButton: 'a[href="/logout"]',
    appSwitcher: '[aria-label="Appswitcher Icon"]',
    createNewButton: 'button#createGlobalItemIconButton',
    dropdownMenu: (item) => `//div[@data-placement="bottom-start"]//span[contains(text(),"${item}")]`,

    logOut() {
        I.moveCursorTo(this.accountButton);
        I.click(this.accountButton);
        I.moveCursorTo(this.logoutButton);
        I.click(this.logoutButton);
    }
}
