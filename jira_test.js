const {password} = require("./configs/creds");
const {email} = require("./configs/creds");
const expect = require ('expect.js');

Feature('jira');

Scenario('Page opened', ({I, loginPage}) => {
    I.amOnPage('/')
    I.see('Войдите в свой аккаунт');
});

Scenario('Successful login', ({I, loginPage}) => {
    I.amOnPage('/')
    loginPage.login(email, password);
    I.see('Ваша работа');
});

Scenario('Successful logout', ({I, loginPage, upperMenuFragment, logOutPage}) => {
    I.amOnPage('/')
    loginPage.login(email, password);
    upperMenuFragment.logOut();
    logOutPage.clickOnLogOutButton();
    I.see('Войдите в свой аккаунт');

});

Scenario('Get space api test', async({I}) => {
    const {data}= await I.sendGetRequest('space');
    const neededSpace = data.results.find(({name}) => name === 'JavaScriptCourse');
    expect(neededSpace.key).to.equal('JAVASCRIPT');

});

Scenario('Get page by Id api test', async({I}) => {
    const id = '29523979';
    const {data}= await I.sendGetRequest(`content/${id}?expand=space,body.view,version,container`);
    expect(data.title).to.equal('Title Keebler');

});
