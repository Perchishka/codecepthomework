const credentials = require("./configs/creds");
const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://javascriptcourseconfluence.atlassian.net/',
      show: true,
      browser: 'chromium',
      waitForAction: "250",
      waitForNavigation: "load"
    },
    REST: {
      endpoint: 'https://javascriptcourseconfluence.atlassian.net/wiki/rest/api/',
      defaultHeaders:{
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + Buffer.from(`${credentials.email}:${credentials.apiToken}`).toString('base64'),
      }
    }
  },
  include: {
    I: './steps_file.js',
    loginPage: './pages/login.js',
    upperMenuFragment: './fragments/upperMenu.js',
    logOutPage: './pages/logOut.js'
  },
  bootstrap: null,
  mocha: {
    'reporterOptions':{
      "reportDir": 'output'
    }
  },
  name: 'codecept',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}
