const { I } = inject();

module.exports = {

    logoutButton: '#logout-submit',
    clickOnLogOutButton() {
        I.waitForElement(this.logoutButton);
        I.click(this.logoutButton);
    }
}
