const { I } = inject();

module.exports = {
    fields: {
        emailField: '#username',
        passwordField: '#password',

    },
    button: {
        loginButton: '#login-submit',
        submitButton: 'button[type="submit"]',
    },
    login(username, password) {
        I.fillField(this.fields.emailField, username);
        I.click(this.button.submitButton);
        I.waitForElement(this.fields.passwordField);
        I.fillField(this.fields.passwordField, password);
        I.click(this.button.loginButton);
    }
}
